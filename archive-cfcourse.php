<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * Template Name: course archive
 */

get_header(); ?>
	<div id="primary" <?php generate_do_element_classes( 'content' ); ?>>
		<main id="main" <?php generate_do_element_classes( 'main' ); ?>>
			<div class="generate-columns-container">

				<header class="page-header">					
					<h1 class="page-title"><?php the_title(); ?></h1>
				</header>

				<?php
					/**
					 * generate_before_main_content hook.
					 *
					 * @since 0.1
					 */
						do_action( 'generate_before_main_content' );
						?>
						</article>
						<?php
						if ( is_user_logged_in() ) :
							?>
							<article class="page generate-columns ">
								<div class="inside-article">
						<?php

							the_content();
							course_archive();
							else :
								echo "<h5 class='notloggedin'>U bent niet ingelogd</h3>";
								wp_login_form();
								echo "<a class='lostpassword' href='/wp-login.php?action=lostpassword'>Wachtwoord vergeten?</a>";
						endif;

							/**
					 * generate_after_main_content hook.
					 *
					 * @since 0.1
					 */
					do_action( 'generate_after_main_content' );
						?>
			</div>
		</main>
	</div>
	<?php 
	do_action( 'generate_after_primary_content_area' );

	generate_construct_sidebars();

	get_footer(); 


