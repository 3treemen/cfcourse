<?php
/**
 * Plugin Name:     Cfcourse
 * Plugin URI:      Custom LMS
 * Description:     Show course based on subscription date
 * Author:          Ronnie Stevens
 * Author URI:      https://zoo.nl
 * Text Domain:     cfcourse
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Cfcourse
 */

// Your code starts here.
include_once('post-types/cfcourse.php');


// function add_taxonomies_to_courses() {
//     register_taxonomy_for_object_type( 'post_tag', 'cfcourse' );
//     register_taxonomy_for_object_type( 'category', 'cfcourse' );
//     }
//    add_action( 'init', 'add_taxonomies_to_courses' );


add_image_size( 'course-thumb', 170, 170, array( 'center', 'center' ) ); // Hard crop left top


/**
 * Add stylesheet to the page
 */
function safely_add_stylesheet() {
    wp_enqueue_style( 'prefix-style', plugins_url('style.css', __FILE__) );
}
add_action( 'wp_enqueue_scripts', 'safely_add_stylesheet' );



/**
 * get the single course template
 */
function get_course_template( $template ) {
    global $post;

    if ( 'cfcourse' === $post->post_type && locate_template( array( 'single-cfcourse.php' ) ) !== $template ) {
        return plugin_dir_path( __FILE__ ) . 'single-cfcourse.php';
    }

    return $template;
}
add_filter( 'single_template', 'get_course_template' );



function single_course() {
    global $post;
    	/* Start the Loop */
        while ( have_posts() ) :
            the_post();

            // Extract variables.           
            $video1 = get_field('video');
            $video2 = get_field('video_2');
            $video3 = get_field('video_3');
            $video4 = get_field('video_4');
            $file = get_field('pdf');

            the_excerpt();
            if( $file ):
                $title = $file['title'];
                $caption = $file['caption'];   
                $icon = $file['icon'];
                $url = $file['url'];
                $plugin_url = plugin_dir_url( __FILE__ );
                $icon = $plugin_url . '/assets/pdf.png';
                $filename = $file['filename'];
                // Display image thumbnail when possible.
                if( $file['type'] == 'image' ):
                    $icon =  $file['sizes']['thumbnail'];
                endif;
            endif;

            if($video3 && !$video4) :
                $countclass = 'three';
            
            else:
                $countclass = 'two';
            endif;
        
?>
            <h2>De video's</h2>

            <div class="videowrapper <?php echo $countclass; ?>">
                <?php if($video1): ?>
                    <div class="embed-container">
                        <?php echo $video1; ?>
                    </div>
                    <?php endif; ?>
                <?php if($video2): ?>
                    <div class="embed-container">
                        <?php echo $video2; ?>
                    </div>
                <?php endif; ?>
                <?php if($video3): ?>
                    <div class="embed-container">
                        <?php echo $video3; ?>
                    </div>
                <?php endif; ?>
                <?php if($video4): ?>
                    <div class="embed-container">
                        <?php echo $video4; ?>
                    </div>
                <?php endif; ?>

            </div>


            <?php if($file): ?>
                <h2>De les</h2>
                <?php echo do_shortcode('[pdfjs-viewer url='. $url .' viewer_height=700px fullscreen=true download=true print=true]'); ?>
            <?php endif; ?> 
            <?php	

        
        endwhile; // End of the loop.
    
        // the_content();
    
}


/**
 * get the archive template
 */
function get_cfcourse_archive_template( $archive_template ) {
    global $post;

    if ( is_post_type_archive ( 'cfcourse' ) ) {
         $archive_template = plugin_dir_path( __FILE__ ) . 'archive-cfcourse.php';
    }
    return $archive_template;
}
add_filter( 'archive_template', 'get_cfcourse_archive_template' ) ;

/* add columns to coursearchive */
add_filter( 'generate_blog_columns','tu_portfolio_columns' );
function tu_portfolio_columns( $columns ) {
    if ( is_post_type_archive( 'cfcourse' ) ) {
        return true;
    }

    return $columns;
}
/* add masonry to coursearchive */
add_filter( 'generate_blog_masonry','cfcourse_masonry' );
function cfcourse_masonry( $masonry ) {
    if ( is_post_type_archive( 'cfcourse' ) ) {
        return 'true';
    }

    return $masonry;
}


/** the course lising, only visible for logged in users
 * it checks the registration date of the user, retrieves the week of the registration
 * and only shows courses from this week and up
 * But first we need to determine how many weeks ago the user was registered
 */
function weeks_since_registration() {

    // get the current date as an object
    $current_date = new DateTime('now');

    // first get the user registration date
    $user_registration_date = get_userdata(get_current_user_id( ))->user_registered;
    // now convert is to an object
    $user_reg = new DateTime( $user_registration_date );

    // having two objects, we can get the diff
    $diff = $user_reg->diff( $current_date );
    
    $days_ago = $diff->days;
    $weeks_ago = floor($days_ago / 7);

    // return $days_ago;
    return $weeks_ago;
}
function get_date() {
	$date_format = get_option('date_format');
	$time_format = get_option('time_format');
	$date = date("{$date_format} {$time_format}", current_time('timestamp'));
	return $date;
}

function course_archive() {
    global $post;
    
    $weeks_ago = weeks_since_registration() + 1;

    $args = array(  
        'post_type' => 'cfcourse',
        'post_status' => 'publish',
        'posts_per_page' => $weeks_ago,
        'meta_key' => 'coursedate', 
        'orderby' => 'meta_value', 
        'order' => 'DESC', 
    );
	
    $user_registration_week = date("W", strtotime(get_userdata(get_current_user_id( ))->user_registered));
   
    $loop = new WP_Query( $args ); 
    $count = 0;

    while ( $loop->have_posts() ) : $loop->the_post();
        $date_string = get_field('coursedate');
        // Create DateTime object from value (formats must match).
        $date = DateTime::createFromFormat('Ymd', $date_string);
        $courseweek = $date->format('W');
        include('archive-article.php'); 

    endwhile;
    
    wp_reset_postdata(); 
}


function cfcourse_add_page_template ($templates) {
    $templates['archive-cfcourse.php'] = 'Course List';
    return $templates;
    }
add_filter ('theme_page_templates', 'cfcourse_add_page_template');

function cfcourse_redirect_page_template ($template) {
    $post = get_post();
    $page_template = get_post_meta( $post->ID, '_wp_page_template', true );
    if ('archive-cfcourse.php' == basename ($page_template))
        $template = WP_PLUGIN_DIR . '/cfcourse/archive-cfcourse.php';
    return $template;
    }
add_filter ('page_template', 'cfcourse_redirect_page_template');




add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
function my_custom_dashboard_widgets() {
        global $wp_meta_boxes;
        wp_add_dashboard_widget('custom_help_widget', 'Cursus Documentatie', 'custom_dashboard_help');
}

function custom_dashboard_help() {
        include 'documentatie.txt';
}

/**
 *	ACF Admin Columns
 *
 */

function add_acf_columns ( $columns ) {
    return array_merge ( $columns, array ( 
      'week' => __ ( 'Week' )
    ) );
  }
  add_filter ( 'manage_cfcourse_posts_columns', 'add_acf_columns' );

   /*
 * Add columns to Cfcourse CPT
 */
 function cfcourse_custom_column ( $column, $post_id ) {
    switch ( $column ) {
      case 'week':
        echo get_post_meta ( $post_id, 'week', true );
        break;
    }
 }
 add_action ( 'manage_cfcourse_posts_custom_column', 'cfcourse_custom_column', 10, 2 );

  /*
 * Add Sortable columns
 */

function cfcourse_register_sortable( $columns ) {
	$columns['week'] = 'week';
	return $columns;
}
add_filter('manage_edit-cfcourse_sortable_columns', 'cfcourse_register_sortable' );


/** redirect after logout */
add_action('wp_logout','ps_redirect_after_logout');
function ps_redirect_after_logout(){
         wp_redirect( '/' );
         exit();
}


/** course widget **/
function coursewidget ($atts, $content = "") {
    
    $content = "<div class='inside-right-sidebar'>";
    if ( is_user_logged_in() ) :
            $content .= "<a href='". wp_logout_url(get_permalink()) . "'>Log uit</a>";
        else :
            $content .= "U bent niet ingelogd<br>";
            $content .= "<a href='/cursussen'>inloggen</a>";

        endif;

    
    // $content .= wp_logout();
    $content .= "</div>";
    return $content;
}
add_shortcode( 'cursistenmenu', 'coursewidget' );



 
//For example, you can paste this into your theme functions.php file
 
function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
		global $template;
		print_r( $template );
	}
}
 
// add_action( 'wp_footer', 'meks_which_template_is_loaded' );

// redirect after login
function my_login_redirect( $redirect_to, $request, $user ) {
    $redirect_to =  home_url();
 
    return $redirect_to;
}
 
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

// don't allow users to access WordPress admin
add_action('admin_init', 'endo_hide_dashboard');
function endo_hide_dashboard() {
 if ( ! current_user_can( 'manage_options' ) && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
  	wp_redirect(home_url()); exit;
  }
}

// remove admin bar from non admin users
add_action('after_setup_theme', 'endo_remove_admin_bar');
function endo_remove_admin_bar() {
	if (!current_user_can('manage_options') && !is_admin()) {
		show_admin_bar(false);
	}
}

/* WooCommerce: The Code Below Removes Checkout Fields */
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
    // unset($fields['billing']['billing_first_name']);
    // unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_phone']);
    unset($fields['order']['order_comments']);
    // unset($fields['billing']['billing_email']);
    unset($fields['account']['account_username']);
    unset($fields['account']['account_password']);
    unset($fields['account']['account_password-2']);
    return $fields;
}



/**
 * remove users that exist 365 days
 */
function my_clearOldUsers() {
    global $wpdb;
    $query = $wpdb->prepare("SELECT ID FROM $wpdb->users WHERE datediff(now(), user_registered) > 364");
    if ($oldUsers = $wpdb->get_results($query, ARRAY_N)) {
        foreach ($oldUsers as $user_id) {
			if($user_id[0] == 1 || $user_id[0] == 6 || $user_id[0] == 7 || $user_id[0] == 8 ) {
				// echo 'not to delete : ' . $user_id[0] . '<br>';
                continue;
			}
			else { 
                // echo 'delete: ' . $user_id[0] . '<br>';
	            wp_delete_user($user_id[0]);
			}
        }
    }
}

add_action('my_dailyClearOut', 'my_clearOldUsers');

if( !wp_next_scheduled( 'my_dailyClearOut' ) ) {  
	wp_schedule_event( time(), 'hourly', 'my_dailyClearOut' );  
}  
