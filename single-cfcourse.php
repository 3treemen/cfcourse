<?php
/**
* Template Name: Single Course
 */

get_header();
?>

<div id="primary" <?php generate_do_element_classes( 'content' ); ?>>
		<main id="main" <?php generate_do_element_classes( 'main' ); ?>>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->
			<div class="entry-content">
				<?php
					if ( is_user_logged_in() ) :
						single_course();
						else :
							echo "You are not logged in";
							wp_login_form();
					endif;		
						/**
			 * generate_after_main_content hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_after_main_content' );
		?>
			</div>
		</article>
	</main>		
	</div>
<?php
	/**
	 * generate_after_primary_content_area hook.
	 *
	 * @since 2.0
	 */
	do_action( 'generate_after_primary_content_area' );

	// generate_construct_sidebars();

	get_footer();
