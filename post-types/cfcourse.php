<?php

/**
 * Registers the `cfcourse` post type.
 */
function cfcourse_init() {
	register_post_type( 'cfcourse', array(
		'labels'                => array(
			'name'                  => __( 'Courses', 'cfcourse' ),
			'singular_name'         => __( 'Cursus', 'cfcourse' ),
			'all_items'             => __( 'All Courses', 'cfcourse' ),
			'archives'              => __( 'Cursus Archives', 'cfcourse' ),
			'attributes'            => __( 'Cursus Attributes', 'cfcourse' ),
			'insert_into_item'      => __( 'Insert into Cursus', 'cfcourse' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Cursus', 'cfcourse' ),
			'featured_image'        => _x( 'Featured Image', 'cfcourse', 'cfcourse' ),
			'set_featured_image'    => _x( 'Set featured image', 'cfcourse', 'cfcourse' ),
			'remove_featured_image' => _x( 'Remove featured image', 'cfcourse', 'cfcourse' ),
			'use_featured_image'    => _x( 'Use as featured image', 'cfcourse', 'cfcourse' ),
			'filter_items_list'     => __( 'Filter Courses list', 'cfcourse' ),
			'items_list_navigation' => __( 'Courses list navigation', 'cfcourse' ),
			'items_list'            => __( 'Courses list', 'cfcourse' ),
			'new_item'              => __( 'New Cursus', 'cfcourse' ),
			'add_new'               => __( 'Add New', 'cfcourse' ),
			'add_new_item'          => __( 'Add New Cursus', 'cfcourse' ),
			'edit_item'             => __( 'Edit Cursus', 'cfcourse' ),
			'view_item'             => __( 'View Cursus', 'cfcourse' ),
			'view_items'            => __( 'View Courses', 'cfcourse' ),
			'search_items'          => __( 'Search Courses', 'cfcourse' ),
			'not_found'             => __( 'No Courses found', 'cfcourse' ),
			'not_found_in_trash'    => __( 'No Courses found in trash', 'cfcourse' ),
			'parent_item_colon'     => __( 'Parent Cursus:', 'cfcourse' ),
			'menu_name'             => __( 'Courses', 'cfcourse' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'thumbnail', 'excerpt' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'cfcourse',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'cfcourse_init' );

/**
 * Sets the post updated messages for the `cfcourse` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `cfcourse` post type.
 */
function cfcourse_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['cfcourse'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Cursus updated. <a target="_blank" href="%s">View Cursus</a>', 'cfcourse' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'cfcourse' ),
		3  => __( 'Custom field deleted.', 'cfcourse' ),
		4  => __( 'Cursus updated.', 'cfcourse' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Cursus restored to revision from %s', 'cfcourse' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Cursus published. <a href="%s">View Cursus</a>', 'cfcourse' ), esc_url( $permalink ) ),
		7  => __( 'Cursus saved.', 'cfcourse' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Cursus submitted. <a target="_blank" href="%s">Preview Cursus</a>', 'cfcourse' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Cursus scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Cursus</a>', 'cfcourse' ),
		date_i18n( __( 'M j, Y @ G:i', 'cfcourse' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Cursus draft updated. <a target="_blank" href="%s">Preview Cursus</a>', 'cfcourse' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'cfcourse_updated_messages' );
