<article id="post-<?php the_ID(); ?>" class="post-758 post type-post status-publish format-standard has-post-thumbnail hentry infinite-scroll-item generate-columns tablet-grid-100 mobile-grid-100 grid-parent grid-100"> 
    <div class="inside-article">
        <header class="entry-header">
            <h3 class="entry-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title();  ?>
                </a>
            </h3>
        </header>
        <div class="inner-wrapper">
            <div class="post-image">
                <a href="<?php the_permalink(); ?>" style="background-image:url('<?php the_post_thumbnail_url('large'); ?>');">
                </a>
            </div>
            <div class="entry-summary" itemprop="text">
                <?php the_excerpt(); ?>
                <span class="readmore">
                    <a href="<?php the_permalink();?>">Naar de les</a>
                </span>
            </div>
        </div>
    </div>
</article>